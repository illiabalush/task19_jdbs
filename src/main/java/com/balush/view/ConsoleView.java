package com.balush.view;

import com.balush.controller.*;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ConsoleView extends View {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Controller> menuItem;
    private Scanner scanner;

    public ConsoleView() {
        scanner = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - airport-flight controller");
        menu.put("2", "2 - airport controller");
        menu.put("3", "3 - city controller");
        menu.put("4", "4 - client controller");
        menu.put("5", "5 - flight controller");
        menu.put("6", "6 - order flight controller");
        menu.put("7", "7 - order seat controller");
        menu.put("8", "8 - plane controller");
        menu.put("9", "9 - plane seat controller");
        menu.put("10", "10 - price controller");
        menu.put("11", "11 - seat class controller");
        menu.put("Q", "Q - exit");
        menuItem = new LinkedHashMap<>();
        menuItem.put("1", new AirportFlightController());
        menuItem.put("2", new AirportListController());
        menuItem.put("3", new CityController());
        menuItem.put("4", new ClientController());
        menuItem.put("5", new FlightController());
        menuItem.put("6", new OrderFlightController());
        menuItem.put("7", new OrderSeatController());
        menuItem.put("8", new PlaneController());
        menuItem.put("9", new PlaneSeatController());
        menuItem.put("10", new PlaneController());
        menuItem.put("11", new SeatClassController());
    }

    @Override
    public void start() {
        String keyMenu;
        do {
            showMenu();
            scanner = new Scanner(System.in);
            logger.info("Please, select menu point:");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                controller = menuItem.get(keyMenu);
                selectControllerItem();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void selectControllerItem() {
        String keyMenu;
        do {
            showControllerMenu();
            scanner = new Scanner(System.in);
            logger.info("Please, select menu point:");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                controller.getMenuItem().get(keyMenu).print();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void showControllerMenu() {
        Map<String, String> controllerMenu = controller.getMenu();
        for (String s : controllerMenu.values()) {
            logger.info(s);
        }
    }

    @Override
    public void showMenu() {
        for (String s : menu.values()) {
            logger.info(s);
        }
    }
}
