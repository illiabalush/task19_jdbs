package com.balush.controller;

import com.balush.model.entity.OrderSeat;
import com.balush.model.service.OrderSeatService;
import com.balush.view.View;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Scanner;

public class OrderSeatController extends Controller {
    public OrderSeatController() {
        scanner = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - show orders seat");
        menu.put("2", "2 - add order seat");
        menuItem = new LinkedHashMap<>();
        menuItem.put("1", this::showOrderSeatTable);
        menuItem.put("2", this::addOrderSeat);
    }

    private void addOrderSeat() throws SQLException {
        new FlightController().getMenuItem().get("1").print();
        View.logger.info("Select flight: ");
        int flightId = scanner.nextInt();
        new SeatClassController().getMenuItem().get("1").print();
        View.logger.info("Select seat class: ");
        int seatClassId = scanner.nextInt();
        OrderSeat orderSeat = new OrderSeat(flightId, seatClassId);
        new OrderSeatService().create(orderSeat);
    }

    private void showOrderSeatTable() {
        try {
            new OrderSeatService().findAll()
                    .forEach(orderSeat -> View.logger.info(orderSeat));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
