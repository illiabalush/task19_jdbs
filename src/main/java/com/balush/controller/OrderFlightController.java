package com.balush.controller;

import com.balush.model.entity.OrderFlight;
import com.balush.model.service.OrderFlightService;
import com.balush.view.View;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Scanner;

public class OrderFlightController extends Controller {
    public OrderFlightController() {
        scanner = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - show orders flight");
        menu.put("2", "2 - start new order");
        menuItem = new LinkedHashMap<>();
        menuItem.put("1", this::showOrderFlightTable);
        menuItem.put("2", this::createNewOrder);
    }

    private void createNewOrder() throws SQLException {
        new FlightController().getMenuItem().get("1").print();
        View.logger.info("Select flight id:");
        int flightId = scanner.nextInt();
        new SeatClassController().getMenuItem().get("1").print();
        View.logger.info("Select seat class id:");
        int seatClassId = scanner.nextInt();
        new ClientController().getMenuItem().get("1").print();
        View.logger.info("Select client id:");
        int clientId = scanner.nextInt();
        OrderFlight orderFlight = new OrderFlight(flightId, seatClassId, clientId);
        new OrderFlightService().tryAddNewOrder(orderFlight);
    }

    private void showOrderFlightTable() {
        try {
            new OrderFlightService().findAll()
                    .forEach(orderFlight -> View.logger.info(orderFlight));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
