package com.balush.controller;

import com.balush.model.entity.PlaneSeat;
import com.balush.model.service.PlaneSeatService;
import com.balush.view.View;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Scanner;

public class PlaneSeatController extends Controller {
    public PlaneSeatController() {
        scanner = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - show planes seats");
        menu.put("2", "2 - add plane seat");
        menuItem = new LinkedHashMap<>();
        menuItem.put("1", this::showPlaneSeatTable);
        menuItem.put("2", this::addPlaneSeat);
    }

    private void addPlaneSeat() throws SQLException {
        new PlaneController().getMenuItem().get("1").print();
        View.logger.info("Select plane: ");
        int planeId = scanner.nextInt();
        new SeatClassController().getMenuItem().get("1").print();
        View.logger.info("Select seat class: ");
        int seatClassId = scanner.nextInt();
        View.logger.info("Enter seat number: ");
        int seatNumber = scanner.nextInt();
        PlaneSeat planeSeat = new PlaneSeat(planeId, seatClassId, seatNumber);
        new PlaneSeatService().create(planeSeat);
    }

    private void showPlaneSeatTable() {
        try {
            new PlaneSeatService().findAll()
                    .forEach(planeSeat -> View.logger.info(planeSeat));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
