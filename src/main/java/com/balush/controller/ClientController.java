package com.balush.controller;

import com.balush.model.entity.ClientList;
import com.balush.model.service.ClientListService;
import com.balush.view.View;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Scanner;

public class ClientController extends Controller {
    public ClientController() {
        scanner = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - show clients");
        menu.put("2", "2 - add client");
        menu.put("3", "3 - remove client");
        menuItem = new LinkedHashMap<>();
        menuItem.put("1", this::showClientsTable);
        menuItem.put("2", this::addClient);
        menuItem.put("3", this::removeClient);
    }

    private void addClient() throws SQLException {
        View.logger.info("Enter name: ");
        String name = scanner.nextLine();
        View.logger.info("Enter surname: ");
        String surname = scanner.nextLine();
        View.logger.info("Enter card number: ");
        String cardNumber = scanner.nextLine();
        View.logger.info("Enter amount of money: ");
        BigDecimal money = scanner.nextBigDecimal();
        ClientList client = new ClientList(name, surname, cardNumber, money);
        new ClientListService().create(client);
    }

    private void removeClient() throws SQLException {
        showClientsTable();
        View.logger.info("Enter id: ");
        int deletedId = scanner.nextInt();
        new ClientListService().deleteById(deletedId);
    }

    private void showClientsTable() {
        try {
            new ClientListService().findAll().forEach(clientList -> View.logger.info(clientList));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
