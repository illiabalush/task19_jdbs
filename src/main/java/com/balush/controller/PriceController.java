package com.balush.controller;

import com.balush.model.service.PriceService;
import com.balush.view.View;

import java.sql.SQLException;
import java.util.LinkedHashMap;

public class PriceController extends Controller {
    public PriceController() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - show prices");
        menuItem = new LinkedHashMap<>();
        menuItem.put("1", this::showPricesTable);
    }

    private void showPricesTable() {
        try {
            new PriceService().findAll().forEach(price -> View.logger.info(price));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
