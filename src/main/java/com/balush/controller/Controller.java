package com.balush.controller;

import com.balush.interfaces.Printable;

import java.util.Map;
import java.util.Scanner;

public abstract class Controller {
    protected Scanner scanner;
    protected Map<String, String> menu;
    protected Map<String, Printable> menuItem;

    public Scanner getScanner() {
        return scanner;
    }

    public void setScanner(Scanner scanner) {
        this.scanner = scanner;
    }

    public Map<String, String> getMenu() {
        return menu;
    }

    public void setMenu(Map<String, String> menu) {
        this.menu = menu;
    }

    public Map<String, Printable> getMenuItem() {
        return menuItem;
    }

    public void setMenuItem(Map<String, Printable> menuItem) {
        this.menuItem = menuItem;
    }
}
