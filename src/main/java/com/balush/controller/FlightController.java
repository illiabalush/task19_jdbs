package com.balush.controller;

import com.balush.model.entity.Flight;
import com.balush.model.service.FlightService;
import com.balush.view.View;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Scanner;

public class FlightController extends Controller {
    public FlightController() {
        scanner = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - show flights");
        menu.put("2", "2 - add flight");
        menu.put("3", "3 - remove flight");
        menuItem = new LinkedHashMap<>();
        menuItem.put("1", this::showFlightTable);
        menuItem.put("2", this::addFlight);
        menuItem.put("3", this::removeFlight);
    }

    private void addFlight() throws SQLException {
        scanner = new Scanner(System.in);
        View.logger.info("Enter flight number:");
        String flightNumber = scanner.nextLine();
        new PlaneController().getMenuItem().get("1").print();
        View.logger.info("Enter plane id:");
        int planeId = scanner.nextInt();
        new CityController().getMenuItem().get("1").print();
        View.logger.info("Enter city from id: ");
        int cityFromId = scanner.nextInt();
        View.logger.info("Enter city to id: ");
        int cityToId = scanner.nextInt();
        String dateFormat = "yyyy/MM/dd HH:mm:ss";
        Date departure = null;
        Date arrival = null;
        try {
            scanner = new Scanner(System.in);
            View.logger.info("Enter departure time in format[yyyy/MM/dd HH:mm:ss]: ");
            departure = new SimpleDateFormat(dateFormat).parse(scanner.nextLine());
            View.logger.info("Enter arrival time in format[yyyy/MM/dd HH:mm:ss]: ");
            arrival = new SimpleDateFormat(dateFormat).parse(scanner.nextLine());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Flight flight = new Flight(flightNumber, planeId, cityFromId,
                cityToId, departure, arrival);
        new FlightService().create(flight);
    }

    private void removeFlight() throws SQLException {
        showFlightTable();
        View.logger.info("Enter id: ");
        int deletedId = scanner.nextInt();
        new FlightService().deleteById(deletedId);
    }

    private void showFlightTable() {
        try {
            new FlightService().findAll().forEach(flight -> View.logger.info(flight));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
