package com.balush.controller;

import com.balush.model.entity.AirportFlight;
import com.balush.model.entity.AirportList;
import com.balush.model.entity.Flight;
import com.balush.model.service.AirportFlightService;
import com.balush.model.service.AirportListService;
import com.balush.model.service.FlightService;
import com.balush.view.View;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.LinkedHashMap;
import java.util.Scanner;

public class AirportFlightController extends Controller {
    public AirportFlightController() {
        scanner = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - show airport-flight table");
        menu.put("2", "2 - connect flight to airport");
        menu.put("3", "3 - delete record");
        menuItem = new LinkedHashMap<>();
        menuItem.put("1", this::showAirportFlightTable);
        menuItem.put("2", this::connectFlightToAirport);
        menuItem.put("3", this::deleteRecord);
    }

    private void showAirportFlightTable() {
        try {
            new AirportFlightService().findAll().forEach(airportFlight ->
                    View.logger.info(airportFlight));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void connectFlightToAirport() throws SQLException {
        View.logger.info("Available flights:");
        new FlightController().getMenuItem().get("1").print();
        View.logger.info("Available airports:");
        new AirportListController().getMenuItem().get("1").print();
        View.logger.info("Choose airport:");
        int airportId = scanner.nextInt();
        View.logger.info("Choose flights:");
        int flightsId = scanner.nextInt();
        new AirportFlightService().create(new AirportFlight(airportId, flightsId));
    }

    private void deleteRecord() throws SQLException {
        View.logger.info("Available flights:");
        new FlightController().getMenuItem().get("1").print();
        View.logger.info("Available airports:");
        new AirportListController().getMenuItem().get("1").print();
        View.logger.info("Choose airport:");
        int airportId = scanner.nextInt();
        View.logger.info("Choose flights:");
        int flightsId = scanner.nextInt();
        AirportList airport = new AirportListService().findById(airportId);
        Flight flight = new FlightService().findById(flightsId);
        new AirportFlightService().deleteByTwoId(airport, flight);
    }
}
