package com.balush.controller;

import com.balush.model.entity.AirportList;
import com.balush.model.service.AirportListService;
import com.balush.view.View;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Scanner;

public class AirportListController extends Controller {
    public AirportListController() {
        scanner = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - show airports");
        menu.put("2", "2 - create airport");
        menu.put("3", "3 - remove airport");
        menuItem = new LinkedHashMap<>();
        menuItem.put("1", this::showAirportsTable);
        menuItem.put("2", this::createAirport);
        menuItem.put("3", this::removeAirport);
    }

    private void showAirportsTable() {
        try {
            new AirportListService().findAll().forEach(airport -> View.logger.info(airport));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void createAirport() throws SQLException {
        View.logger.info("Enter name airport: ");
        String name = scanner.nextLine();
        AirportList airport = new AirportList(name);
        new AirportListService().create(airport);
    }

    private void removeAirport() throws SQLException {
        showAirportsTable();
        View.logger.info("Enter id airport: ");
        int deletedId = scanner.nextInt();
        new AirportListService().deleteById(deletedId);
    }
}
