package com.balush.controller;

import com.balush.model.entity.City;
import com.balush.model.service.CityService;
import com.balush.view.View;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Scanner;

public class CityController extends Controller {
    public CityController() {
        scanner = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - show cities");
        menu.put("2", "2 - add city");
        menu.put("3", "3 - remove city");
        menuItem = new LinkedHashMap<>();
        menuItem.put("1", this::showCityTable);
        menuItem.put("2", this::addCity);
        menuItem.put("3", this::removeCity);
    }

    private void addCity() throws SQLException {
        View.logger.info("Enter city name");
        String name = scanner.nextLine();
        City city = new City(name);
        new CityService().create(city);
    }

    private void removeCity() throws SQLException {
        showCityTable();
        View.logger.info("Enter id: ");
        int deletedId = scanner.nextInt();
        new CityService().deleteById(deletedId);
    }

    private void showCityTable() {
        try {
            new CityService().findAll().forEach(city -> View.logger.info(city));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
