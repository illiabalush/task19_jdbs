package com.balush.controller;

import com.balush.model.service.SeatClassService;
import com.balush.view.View;

import java.sql.SQLException;
import java.util.LinkedHashMap;

public class SeatClassController extends Controller {
    public SeatClassController() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - show seats classes");
        menuItem = new LinkedHashMap<>();
        menuItem.put("1", this::showSeatClassTable);
    }

    private void showSeatClassTable() {
        try {
            new SeatClassService().findAll()
                    .forEach(seatClass -> View.logger.info(seatClass));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
