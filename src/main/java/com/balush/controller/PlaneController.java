package com.balush.controller;

import com.balush.model.service.PlaneService;
import com.balush.view.View;

import java.sql.SQLException;
import java.util.LinkedHashMap;

public class PlaneController extends Controller {
    public PlaneController() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - show planes");
        menuItem = new LinkedHashMap<>();
        menuItem.put("1", this::showPlanesTable);
    }

    private void showPlanesTable() {
        try {
            new PlaneService().findAll().forEach(plane -> View.logger.info(plane));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
