package com.balush.model.DAO.interfaces;

import com.balush.model.entity.ClientList;

import java.sql.SQLException;

public interface ClientListDAO extends CommonDAO<ClientList, Integer, Object> {
    int updateMoney (ClientList clientList) throws SQLException;

    ClientList findById (Integer id) throws SQLException;
}
