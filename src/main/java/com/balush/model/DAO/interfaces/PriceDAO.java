package com.balush.model.DAO.interfaces;

import com.balush.model.entity.Plane;
import com.balush.model.entity.Price;
import com.balush.model.entity.SeatClass;

public interface PriceDAO extends CommonDAO<Price, Plane, SeatClass> {
}
