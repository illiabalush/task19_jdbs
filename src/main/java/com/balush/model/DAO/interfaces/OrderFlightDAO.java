package com.balush.model.DAO.interfaces;

import com.balush.model.entity.OrderFlight;

public interface OrderFlightDAO extends CommonDAO<OrderFlight, Integer, Object> {
}
