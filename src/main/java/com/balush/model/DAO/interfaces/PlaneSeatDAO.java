package com.balush.model.DAO.interfaces;

import com.balush.model.entity.Plane;
import com.balush.model.entity.PlaneSeat;
import com.balush.model.entity.SeatClass;

public interface PlaneSeatDAO extends CommonDAO<PlaneSeat, Plane, SeatClass> {
}
