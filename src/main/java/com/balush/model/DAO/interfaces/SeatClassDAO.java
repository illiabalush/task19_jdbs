package com.balush.model.DAO.interfaces;

import com.balush.model.entity.SeatClass;

public interface SeatClassDAO extends CommonDAO<SeatClass, Integer, Object> {
}
