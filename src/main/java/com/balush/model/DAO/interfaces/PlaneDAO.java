package com.balush.model.DAO.interfaces;

import com.balush.model.entity.Plane;

public interface PlaneDAO extends CommonDAO<Plane, Integer, Object> {
}
