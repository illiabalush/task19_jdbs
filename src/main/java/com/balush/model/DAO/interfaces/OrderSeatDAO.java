package com.balush.model.DAO.interfaces;

import com.balush.model.entity.Flight;
import com.balush.model.entity.OrderSeat;
import com.balush.model.entity.SeatClass;

public interface OrderSeatDAO extends CommonDAO<OrderSeat, Flight, SeatClass>{
}
