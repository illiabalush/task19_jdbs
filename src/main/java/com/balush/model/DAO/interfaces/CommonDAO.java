package com.balush.model.DAO.interfaces;

import java.sql.SQLException;
import java.util.List;

public interface CommonDAO<V, ID, ID2> {
    default List<V> findAll() throws SQLException {
        return null;
    }

    default V findById(ID id) throws SQLException {
        return null;
    }

    default V findByTwoId(ID id1, ID2 id2) throws SQLException {
        return null;
    }

    default int deleteById(ID id) throws SQLException {
        return 0;
    }

    default int deleteByTwoId(ID id1, ID2 id2) throws SQLException {
        return 0;
    }

    default int create(V entity) throws SQLException {
        return 0;
    }

    default int update(V entity) throws SQLException {
        return 0;
    }
}
