package com.balush.model.DAO.interfaces;

import com.balush.model.entity.Flight;

import java.sql.SQLException;

public interface FlightDAO extends CommonDAO<Flight, Integer, Object> {
    Flight getFlightByNumberFlight(String numberFlight) throws SQLException;
}
