package com.balush.model.DAO.interfaces;

import com.balush.model.entity.AirportFlight;
import com.balush.model.entity.AirportList;
import com.balush.model.entity.Flight;

public interface AirportFlightDAO extends CommonDAO<AirportFlight, AirportList, Flight> {
}
