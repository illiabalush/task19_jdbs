package com.balush.model.DAO.interfaces;

import com.balush.model.entity.City;

public interface CityDAO extends CommonDAO<City, Integer, Object> {
}
