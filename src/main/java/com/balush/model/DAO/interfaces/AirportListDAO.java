package com.balush.model.DAO.interfaces;

import com.balush.model.entity.AirportList;

public interface AirportListDAO extends CommonDAO<AirportList, Integer, Object> {
}
