package com.balush.model.DAO.implementation;

import com.balush.model.DAO.interfaces.PlaneDAO;
import com.balush.model.entity.Plane;
import com.balush.model.manager.ConnectionManager;
import com.balush.model.transformer.Transformer;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class PlaneDAOImpl implements PlaneDAO {
    private final String FIND_ALL = "SELECT * FROM plane";
    private final String DELETE = "DELETE FROM plane WHERE id=?";
    private final String CREATE = "INSERT plane (model) VALUES (?)";
    private final String UPDATE = "UPDATE plane SET model=? WHERE id=?";

    @Override
    public List<Plane> findAll() throws SQLException {
        List<Plane> planes = new LinkedList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_ALL);
            while (resultSet.next()) {
                planes.add(((Plane) new Transformer(Plane.class).convertResultSetToEntity(resultSet)));
            }
        }
        return planes;
    }

    @Override
    public Plane findById(Integer id) throws SQLException {
        String FIND_BY_ID = "SELECT * FROM plane WHERE id=" + id;
        Connection connection = ConnectionManager.getConnection();
        Plane plane;
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_BY_ID);
            plane = ((Plane) new Transformer(Plane.class)
                    .convertResultSetToEntity(resultSet));
        }
        return plane;
    }

    @Override
    public int deleteById(Integer deletedID) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.setInt(1, deletedID);
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(Plane plane) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
            preparedStatement.setString(1, plane.getModel());
            preparedStatement.setInt(1, plane.getId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int create(Plane plane) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(CREATE)) {
            preparedStatement.setString(1, plane.getModel());
            return preparedStatement.executeUpdate();
        }
    }
}
