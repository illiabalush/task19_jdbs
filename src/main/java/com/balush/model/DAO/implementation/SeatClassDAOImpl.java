package com.balush.model.DAO.implementation;

import com.balush.model.DAO.interfaces.SeatClassDAO;
import com.balush.model.entity.SeatClass;
import com.balush.model.manager.ConnectionManager;
import com.balush.model.transformer.Transformer;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class SeatClassDAOImpl implements SeatClassDAO {
    private final String FIND_ALL = "SELECT * FROM seat_class";
    private final String DELETE = "DELETE FROM seat_class WHERE id=?";
    private final String CREATE = "INSERT seat_class (category) VALUES (?)";
    private final String UPDATE = "UPDATE seat_class SET category=? WHERE id=?";

    @Override
    public List<SeatClass> findAll() throws SQLException {
        List<SeatClass> seatClasses = new LinkedList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_ALL);
            while (resultSet.next()) {
                seatClasses.add(((SeatClass) new Transformer(SeatClass.class)
                        .convertResultSetToEntity(resultSet)));
            }
        }
        return seatClasses;
    }

    @Override
    public SeatClass findById(Integer id) throws SQLException {
        String FIND_BY_ID = "SELECT * FROM seat_class WHERE id=" + id;
        Connection connection = ConnectionManager.getConnection();
        SeatClass seatClass;
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_BY_ID);
            seatClass = ((SeatClass) new Transformer(SeatClass.class)
                    .convertResultSetToEntity(resultSet));
        }
        return seatClass;
    }

    @Override
    public int deleteById(Integer deletedID) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.setInt(1, deletedID);
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(SeatClass seatClass) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
            preparedStatement.setString(1, seatClass.getCategory());
            preparedStatement.setInt(1, seatClass.getId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int create(SeatClass seatClass) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(CREATE)) {
            preparedStatement.setString(1, seatClass.getCategory());
            return preparedStatement.executeUpdate();
        }
    }
}
