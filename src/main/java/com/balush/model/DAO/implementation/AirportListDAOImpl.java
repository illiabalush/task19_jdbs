package com.balush.model.DAO.implementation;

import com.balush.model.DAO.interfaces.AirportListDAO;
import com.balush.model.entity.AirportList;
import com.balush.model.manager.ConnectionManager;
import com.balush.model.transformer.Transformer;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class AirportListDAOImpl implements AirportListDAO {
    private final String FIND_ALL = "SELECT * FROM airport_list";
    private final String DELETE = "DELETE FROM airport_list WHERE id=?";
    private final String CREATE = "INSERT airport_list (airport_name) VALUES (?)";
    private final String UPDATE = "UPDATE airport_list SET airport_name=? WHERE id=?";

    @Override
    public List<AirportList> findAll() throws SQLException {
        List<AirportList> airportLists = new LinkedList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_ALL);
            while (resultSet.next()) {
                airportLists.add(((AirportList) new Transformer(AirportList.class)
                        .convertResultSetToEntity(resultSet)));
            }
        }
        return airportLists;
    }

    @Override
    public int deleteById(Integer deletedID) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.setInt(1, deletedID);
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public AirportList findById(Integer id) throws SQLException {
        String FIND_BY_ID = "SELECT * FROM airport_list WHERE id=" + id;
        AirportList airportList = null;
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_BY_ID);
            airportList = ((AirportList) new Transformer(AirportList.class)
                    .convertResultSetToEntity(resultSet));
        }
        return airportList;
    }

    @Override
    public int update(AirportList entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
            preparedStatement.setString(1, entity.getAirportName());
            preparedStatement.setInt(2, entity.getId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int create(AirportList entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(CREATE)) {
            preparedStatement.setString(1, entity.getAirportName());
            return preparedStatement.executeUpdate();
        }
    }
}
