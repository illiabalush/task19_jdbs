package com.balush.model.DAO.implementation;

import com.balush.model.DAO.interfaces.OrderSeatDAO;
import com.balush.model.entity.Flight;
import com.balush.model.entity.OrderSeat;
import com.balush.model.entity.SeatClass;
import com.balush.model.manager.ConnectionManager;
import com.balush.model.transformer.Transformer;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class OrderSeatDAOImpl implements OrderSeatDAO {
    private final String FIND_ALL = "SELECT * FROM order_seat";
    private final String DELETE = "DELETE FROM order_seat WHERE flight_id=? AND seat_class_id=?";
    private final String CREATE = "INSERT order_seat (flight_id, seat_class_id, number_free)" +
            " VALUES (?, ?, ?)";
    private final String UPDATE = "UPDATE order_seat SET number_free=?" +
            " WHERE flight_id=? AND seat_class_id=?";

    @Override
    public List<OrderSeat> findAll() throws SQLException {
        List<OrderSeat> orderSeats = new LinkedList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_ALL);
            while (resultSet.next()) {
                orderSeats.add(((OrderSeat) new Transformer(OrderSeat.class)
                        .convertResultSetToEntity(resultSet)));
            }
        }
        return orderSeats;
    }

    @Override
    public OrderSeat findByTwoId(Flight flight, SeatClass seatClass) throws SQLException {
        OrderSeat orderSeat;
        String FIND_BY_TWO_PK = "SELECT * FROM order_seat" +
                " WHERE flight_id=" + flight.getId() + " AND seat_class_id=" + seatClass.getId();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_BY_TWO_PK);
            orderSeat = ((OrderSeat) new Transformer(OrderSeat.class).convertResultSetToEntity(resultSet));
        }
        return orderSeat;
    }

    @Override
    public int deleteByTwoId(Flight flight, SeatClass seatClass) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.setInt(1, flight.getId());
            preparedStatement.setInt(2, seatClass.getId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int create(OrderSeat entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(CREATE)) {
            preparedStatement.setInt(1, entity.getFlightId());
            preparedStatement.setInt(2, entity.getSeatClassId());
            preparedStatement.setInt(3, entity.getNumberFreeSeats());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(OrderSeat entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
            preparedStatement.setInt(1, entity.getNumberFreeSeats());
            preparedStatement.setInt(2, entity.getFlightId());
            preparedStatement.setInt(3, entity.getSeatClassId());
            return preparedStatement.executeUpdate();
        }
    }
}
