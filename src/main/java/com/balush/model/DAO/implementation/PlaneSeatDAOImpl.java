package com.balush.model.DAO.implementation;

import com.balush.model.DAO.interfaces.PlaneSeatDAO;
import com.balush.model.entity.*;
import com.balush.model.manager.ConnectionManager;
import com.balush.model.transformer.Transformer;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class PlaneSeatDAOImpl implements PlaneSeatDAO {
    private final String FIND_ALL = "SELECT * FROM plane_seat";
    private final String DELETE = "DELETE FROM plane_seat WHERE plane_id=? AND seat_class_id=?";
    private final String CREATE = "INSERT plane_seat (plane_id, seat_class_id, seat_number)" +
            " VALUES (?, ?, ?)";
    private final String UPDATE = "UPDATE plane_seat SET seat_number=?" +
            " WHERE plane_id=? AND seat_class_id=?";

    @Override
    public List<PlaneSeat> findAll() throws SQLException {
        List<PlaneSeat> planeSeats = new LinkedList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_ALL);
            while (resultSet.next()) {
                planeSeats.add(((PlaneSeat) new Transformer(PlaneSeat.class)
                        .convertResultSetToEntity(resultSet)));
            }
        }
        return planeSeats;
    }

    @Override
    public PlaneSeat findByTwoId(Plane plane, SeatClass seatClass) throws SQLException {
        PlaneSeat planeSeat;
        String FIND_BY_TWO_PK = "SELECT * FROM plane_seat" +
                " WHERE plane_id=" + plane.getId() + " AND seat_class_id=" + seatClass.getId();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_BY_TWO_PK);
            planeSeat = ((PlaneSeat) new Transformer(PlaneSeat.class).convertResultSetToEntity(resultSet));
        }
        return planeSeat;
    }

    @Override
    public int deleteByTwoId(Plane plane, SeatClass seatClass) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.setInt(1, plane.getId());
            preparedStatement.setInt(2, seatClass.getId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int create(PlaneSeat entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(CREATE)) {
            preparedStatement.setInt(1, entity.getPlaneId());
            preparedStatement.setInt(2, entity.getSeatClassId());
            preparedStatement.setInt(3, entity.getSeatNumber());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(PlaneSeat entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
            preparedStatement.setInt(1, entity.getSeatNumber());
            preparedStatement.setInt(2, entity.getPlaneId());
            preparedStatement.setInt(3, entity.getSeatClassId());
            return preparedStatement.executeUpdate();
        }
    }
}
