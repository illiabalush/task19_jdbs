package com.balush.model.DAO.implementation;

import com.balush.model.DAO.interfaces.FlightDAO;
import com.balush.model.entity.Flight;
import com.balush.model.manager.ConnectionManager;
import com.balush.model.transformer.Transformer;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class FlightDAOImpl implements FlightDAO {
    private final String FIND_ALL = "SELECT * FROM flight";
    private final String DELETE = "DELETE FROM flight WHERE id=?";
    private final String CREATE = "INSERT flight (flight_number, plane_id, from_city_id," +
            "to_city_id, departure_date, arrival_date) VALUES (?, ?, ?, ?, ?, ?)";
    private final String UPDATE = "UPDATE flight SET flight_number=?, plane_id=?, from_city_id=?," +
            "to_city_id=?, departure_date=?, arrival_date=? WHERE id=?";

    @Override
    public List<Flight> findAll() throws SQLException {
        List<Flight> flightList = new LinkedList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_ALL);
            while (resultSet.next()) {
                flightList.add(((Flight) new Transformer(Flight.class)
                        .convertResultSetToEntity(resultSet)));
            }
        }
        return flightList;
    }

    @Override
    public Flight getFlightByNumberFlight(String numberFlight) throws SQLException {
        String FIND_BY_NUMBER = "SELECT * FROM flight WHERE flight_number=" + numberFlight;
        Flight flight = null;
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_BY_NUMBER);
            flight = ((Flight) new Transformer(Flight.class).convertResultSetToEntity(resultSet));
        }
        return flight;
    }

    @Override
    public Flight findById(Integer id) throws SQLException {
        String FIND_BY_ID = "SELECT * FROM flight WHERE id=" + id;
        Flight flight = null;
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_BY_ID);
            flight = ((Flight) new Transformer(Flight.class).convertResultSetToEntity(resultSet));
        }
        return flight;
    }

    @Override
    public int deleteById(Integer deletedID) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.setInt(1, deletedID);
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(Flight flight) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
            preparedStatement.setString(1, flight.getFlightNumber());
            preparedStatement.setInt(2, flight.getPlaneId());
            preparedStatement.setInt(3, flight.getCityFromId());
            preparedStatement.setInt(4, flight.getCityToId());
            preparedStatement.setDate(5, new Date(flight.getDepartureDate().getTime()));
            preparedStatement.setDate(6, new Date(flight.getArrivalDate().getTime()));
            preparedStatement.setInt(7, flight.getId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int create(Flight flight) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(CREATE)) {
            preparedStatement.setString(1, flight.getFlightNumber());
            preparedStatement.setInt(2, flight.getPlaneId());
            preparedStatement.setInt(3, flight.getCityFromId());
            preparedStatement.setInt(4, flight.getCityToId());
            preparedStatement.setDate(5, new Date(flight.getDepartureDate().getTime()));
            preparedStatement.setDate(6, new Date(flight.getArrivalDate().getTime()));
            return preparedStatement.executeUpdate();
        }
    }
}
