package com.balush.model.DAO.implementation;

import com.balush.model.DAO.interfaces.PriceDAO;
import com.balush.model.entity.Plane;
import com.balush.model.entity.Price;
import com.balush.model.entity.SeatClass;
import com.balush.model.manager.ConnectionManager;
import com.balush.model.transformer.Transformer;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class PriceDAOImpl implements PriceDAO {
    private final String FIND_ALL = "SELECT * FROM price";
    private final String DELETE = "DELETE FROM price WHERE plane_id=? AND seat_class_id=?";
    private final String CREATE = "INSERT price (plane_id, seat_class_id, price)" +
            " VALUES (?, ?, ?)";
    private final String UPDATE = "UPDATE price SET price=?" +
            " WHERE plane_id=? AND seat_class_id=?";

    @Override
    public List<Price> findAll() throws SQLException {
        List<Price> prices = new LinkedList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_ALL);
            while (resultSet.next()) {
                prices.add(((Price) new Transformer(Price.class)
                        .convertResultSetToEntity(resultSet)));
            }
        }
        return prices;
    }

    @Override
    public Price findByTwoId(Plane plane, SeatClass seatClass) throws SQLException {
        Price price;
        String FIND_BY_TWO_PK = "SELECT * FROM price" +
                " WHERE plane_id=" + plane.getId() + " AND seat_class_id=" + seatClass.getId();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_BY_TWO_PK);
            price = ((Price) new Transformer(Price.class).convertResultSetToEntity(resultSet));
        }
        return price;
    }

    @Override
    public int deleteByTwoId(Plane plane, SeatClass seatClass) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.setInt(1, plane.getId());
            preparedStatement.setInt(2, seatClass.getId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int create(Price entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(CREATE)) {
            preparedStatement.setInt(1, entity.getPlaneId());
            preparedStatement.setInt(2, entity.getSeatClassId());
            preparedStatement.setBigDecimal(3, entity.getPrice());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(Price entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
            preparedStatement.setBigDecimal(1, entity.getPrice());
            preparedStatement.setInt(2, entity.getPlaneId());
            preparedStatement.setInt(3, entity.getSeatClassId());
            return preparedStatement.executeUpdate();
        }
    }
}
