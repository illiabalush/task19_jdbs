package com.balush.model.DAO.implementation;

import com.balush.model.DAO.interfaces.AirportFlightDAO;
import com.balush.model.entity.AirportFlight;
import com.balush.model.entity.AirportList;
import com.balush.model.entity.Flight;
import com.balush.model.manager.ConnectionManager;
import com.balush.model.transformer.Transformer;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class AirportFlightDAOImpl implements AirportFlightDAO {
    private final String FIND_ALL = "SELECT * FROM airport_flight";
    private final String DELETE = "DELETE FROM airport_flight WHERE airport_id=? AND flight_id=?";
    private final String CREATE = "INSERT airport_flight (airport_id, flight_id) VALUES (?, ?)";

    @Override
    public List<AirportFlight> findAll() throws SQLException {
        List<AirportFlight> airportFlights = new LinkedList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_ALL);
            while (resultSet.next()) {
                airportFlights.add(((AirportFlight) new Transformer(AirportFlight.class)
                        .convertResultSetToEntity(resultSet)));
            }
        }
        return airportFlights;
    }

    @Override
    public AirportFlight findByTwoId(AirportList airportList, Flight flight) throws SQLException {
        AirportFlight airportFlight;
        String FIND_BY_TWO_PK = "SELECT * FROM airport_flight WHERE airport_id="
                + airportList.getId() + "AND flight_id=" + flight.getId();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_BY_TWO_PK);
            airportFlight = ((AirportFlight) new Transformer(AirportFlight.class)
                    .convertResultSetToEntity(resultSet));
        }
        return airportFlight;
    }

    @Override
    public int deleteByTwoId(AirportList airportList, Flight flight) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.setInt(1, airportList.getId());
            preparedStatement.setInt(2, flight.getId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int create(AirportFlight entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(CREATE)) {
            preparedStatement.setInt(1, entity.getAirportId());
            preparedStatement.setInt(2, entity.getFlightId());
            return preparedStatement.executeUpdate();
        }
    }
}
