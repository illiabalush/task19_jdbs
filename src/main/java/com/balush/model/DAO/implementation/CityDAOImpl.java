package com.balush.model.DAO.implementation;

import com.balush.model.DAO.interfaces.CityDAO;
import com.balush.model.entity.City;
import com.balush.model.entity.ClientList;
import com.balush.model.manager.ConnectionManager;
import com.balush.model.transformer.Transformer;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class CityDAOImpl implements CityDAO {
    private final String FIND_ALL = "SELECT * FROM city";
    private final String DELETE = "DELETE FROM city WHERE id=?";
    private final String CREATE = "INSERT city (city) VALUES (?)";
    private final String UPDATE = "UPDATE city SET city=? WHERE id=?";

    @Override
    public List<City> findAll() throws SQLException {
        List<City> cityList = new LinkedList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_ALL);
            while (resultSet.next()) {
                cityList.add(((City) new Transformer(City.class).convertResultSetToEntity(resultSet)));
            }
        }
        return cityList;
    }

    @Override
    public int deleteById(Integer deletedID) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.setInt(1, deletedID);
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public City findById(Integer id) throws SQLException {
        String FIND_BY_ID = "SELECT * FROM city WHERE id=" + id;
        Connection connection = ConnectionManager.getConnection();
        City city = null;
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_BY_ID);
            city = ((City) new Transformer(City.class).convertResultSetToEntity(resultSet));
        }
        return city;
    }

    @Override
    public int update(City city) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
            preparedStatement.setString(1, city.getCity());
            preparedStatement.setInt(2, city.getId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int create(City city) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(CREATE)) {
            preparedStatement.setString(1, city.getCity());
            return preparedStatement.executeUpdate();
        }
    }
}
