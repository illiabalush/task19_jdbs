package com.balush.model.DAO.implementation;

import com.balush.model.DAO.interfaces.ClientListDAO;
import com.balush.model.entity.ClientList;
import com.balush.model.manager.ConnectionManager;
import com.balush.model.transformer.Transformer;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class ClientListDAOImpl implements ClientListDAO {
    private final String FIND_ALL = "SELECT * FROM client_list";
    private final String DELETE = "DELETE FROM client_list WHERE id=?";
    private final String CREATE = "INSERT client_list (client_name, client_surname, card_number," +
            "money) VALUES (?, ?, ?, ?)";
    private final String UPDATE = "UPDATE client_list SET client_name=?, client_surname=?, card_number=?," +
            "money=? WHERE id=?";
    private final String UPDATE_MONEY = "UPDATE client_list SET money=? WHERE id=?";

    @Override
    public List<ClientList> findAll() throws SQLException {
        List<ClientList> clientLists = new LinkedList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_ALL);
            while (resultSet.next()) {
                clientLists.add(((ClientList) new Transformer(ClientList.class)
                        .convertResultSetToEntity(resultSet)));
            }
        }
        return clientLists;
    }

    @Override
    public ClientList findById(Integer id) throws SQLException {
        String FIND_BY_ID = "SELECT * FROM client_list WHERE id=" + id;
        Connection connection = ConnectionManager.getConnection();
        ClientList clientList;
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_BY_ID);
            clientList = ((ClientList) new Transformer(ClientList.class)
                    .convertResultSetToEntity(resultSet));
        }
        return clientList;
    }

    @Override
    public int deleteById(Integer deletedID) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.setInt(1, deletedID);
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(ClientList clientList) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
            preparedStatement.setString(1, clientList.getName());
            preparedStatement.setString(2, clientList.getSurname());
            preparedStatement.setString(3, clientList.getCardNumber());
            preparedStatement.setBigDecimal(4, clientList.getMoney());
            preparedStatement.setInt(5, clientList.getId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int updateMoney(ClientList clientList) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_MONEY)) {
            preparedStatement.setBigDecimal(1, clientList.getMoney());
            preparedStatement.setInt(2, clientList.getId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int create(ClientList clientList) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(CREATE)) {
            preparedStatement.setString(1, clientList.getName());
            preparedStatement.setString(2, clientList.getSurname());
            preparedStatement.setString(3, clientList.getCardNumber());
            preparedStatement.setBigDecimal(4, clientList.getMoney());
            return preparedStatement.executeUpdate();
        }
    }
}
