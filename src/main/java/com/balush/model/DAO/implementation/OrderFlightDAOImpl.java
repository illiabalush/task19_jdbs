package com.balush.model.DAO.implementation;

import com.balush.model.DAO.interfaces.OrderFlightDAO;
import com.balush.model.entity.OrderFlight;
import com.balush.model.manager.ConnectionManager;
import com.balush.model.transformer.Transformer;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class OrderFlightDAOImpl implements OrderFlightDAO {
    private final String FIND_ALL = "SELECT * FROM order_flight";
    private final String DELETE = "DELETE FROM order_flight WHERE id=?";
    private final String CREATE = "INSERT order_flight (flight_id, seat_class_id, client_id)" +
            " VALUES (?, ?, ?)";
    private final String UPDATE = "UPDATE order_flight SET flight_id=?, seat_class_id=?, client_id=?" +
            " WHERE id=?";

    @Override
    public List<OrderFlight> findAll() throws SQLException {
        List<OrderFlight> orderFlights = new LinkedList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_ALL);
            while (resultSet.next()) {
                orderFlights.add(((OrderFlight) new Transformer(OrderFlight.class)
                        .convertResultSetToEntity(resultSet)));
            }
        }
        return orderFlights;
    }

    @Override
    public int deleteById(Integer deletedID) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.setInt(1, deletedID);
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public OrderFlight findById(Integer id) throws SQLException {
        String FIND_BY_ID = "SELECT * FROM order_flight WHERE id=" + id;
        Connection connection = ConnectionManager.getConnection();
        OrderFlight orderFlight;
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_BY_ID);
            orderFlight = ((OrderFlight) new Transformer(OrderFlight.class)
                    .convertResultSetToEntity(resultSet));
        }
        return orderFlight;
    }

    @Override
    public int update(OrderFlight orderFlight) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE)) {
            preparedStatement.setInt(1, orderFlight.getFlightId());
            preparedStatement.setInt(2, orderFlight.getSeatClassId());
            preparedStatement.setInt(3, orderFlight.getClientId());
            preparedStatement.setInt(4, orderFlight.getId());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int create(OrderFlight orderFlight) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(CREATE)) {
            preparedStatement.setInt(1, orderFlight.getFlightId());
            preparedStatement.setInt(2, orderFlight.getSeatClassId());
            preparedStatement.setInt(3, orderFlight.getClientId());
            return preparedStatement.executeUpdate();
        }
    }
}
