package com.balush.model.service;

import com.balush.model.DAO.implementation.AirportListDAOImpl;
import com.balush.model.entity.AirportList;

import java.sql.SQLException;
import java.util.List;

public class AirportListService {
    public List<AirportList> findAll() throws SQLException {
        return new AirportListDAOImpl().findAll();
    }

    public int deleteById(Integer deletedID) throws SQLException {
        return new AirportListDAOImpl().deleteById(deletedID);
    }

    public int update(AirportList entity) throws SQLException {
        return new AirportListDAOImpl().update(entity);
    }

    public AirportList findById(Integer id) throws SQLException {
        return new AirportListDAOImpl().findById(id);
    }

    public int create(AirportList entity) throws SQLException {
        return new AirportListDAOImpl().create(entity);
    }
}
