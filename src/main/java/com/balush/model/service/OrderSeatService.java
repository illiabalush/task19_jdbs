package com.balush.model.service;

import com.balush.model.DAO.implementation.OrderSeatDAOImpl;
import com.balush.model.entity.*;

import java.sql.SQLException;
import java.util.List;

public class OrderSeatService {
    public List<OrderSeat> findAll() throws SQLException {
        return new OrderSeatDAOImpl().findAll();
    }

    public OrderSeat findByTwoId(Flight flight, SeatClass seatClass) throws SQLException {
        return new OrderSeatDAOImpl().findByTwoId(flight, seatClass);
    }

    public int deleteByTwoId(Flight flight, SeatClass seatClass) throws SQLException {
        return new OrderSeatDAOImpl().deleteByTwoId(flight, seatClass);
    }

    public int create(OrderSeat entity) throws SQLException {
        System.out.println(entity.getFlightId() + " " + entity.getSeatClassId() + " " + entity.getNumberFreeSeats());
        Flight flight = new FlightService().findById(entity.getFlightId());
        Plane plane = new PlaneService().findById(flight.getPlaneId());
        SeatClass seatClass = new SeatClassService().findById(entity.getSeatClassId());
        PlaneSeat planeSeat = new PlaneSeatService().findByTwoId(plane, seatClass);
        entity.setNumberFreeSeats(planeSeat.getSeatNumber());
        return new OrderSeatDAOImpl().create(entity);
    }

    public int update(OrderSeat entity) throws SQLException {
        return new OrderSeatDAOImpl().update(entity);
    }
}
