package com.balush.model.service;

import com.balush.model.DAO.implementation.ClientListDAOImpl;
import com.balush.model.entity.ClientList;

import java.sql.SQLException;
import java.util.List;

public class ClientListService {
    public List<ClientList> findAll() throws SQLException {
        return new ClientListDAOImpl().findAll();
    }

    public int deleteById(Integer deletedID) throws SQLException {
        return new ClientListDAOImpl().deleteById(deletedID);
    }

    public int update(ClientList clientList) throws SQLException {
        return new ClientListDAOImpl().update(clientList);
    }

    public int updateMoney(ClientList clientList) throws SQLException {
        return new ClientListDAOImpl().updateMoney(clientList);
    }

    public ClientList findById(Integer id) throws SQLException {
        return new ClientListDAOImpl().findById(id);
    }

    public int create(ClientList clientList) throws SQLException {
        return new ClientListDAOImpl().create(clientList);
    }

}
