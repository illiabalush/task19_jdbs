package com.balush.model.service;

import com.balush.model.DAO.implementation.OrderFlightDAOImpl;
import com.balush.model.entity.*;
import com.balush.model.manager.ConnectionManager;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class OrderFlightService {
    public List<OrderFlight> findAll() throws SQLException {
        return new OrderFlightDAOImpl().findAll();
    }

    public int deleteById(Integer deletedID) throws SQLException {
        return new OrderFlightDAOImpl().deleteById(deletedID);
    }

    public OrderFlight findById(Integer id) throws SQLException {
        return new OrderFlightDAOImpl().findById(id);
    }

    public int update(OrderFlight orderFlight) throws SQLException {
        return new OrderFlightDAOImpl().update(orderFlight);
    }

    private int create(OrderFlight orderFlight) throws SQLException {
        return new OrderFlightDAOImpl().create(orderFlight);
    }

    public int tryAddNewOrder(OrderFlight orderFlight) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try {
            connection.setAutoCommit(false);
            Flight flight = new FlightService().findById(orderFlight.getFlightId());
            Plane plane = new PlaneService().findById(flight.getPlaneId());
            ClientList client = new ClientListService().findById(orderFlight.getClientId());
            SeatClass seatClass = new SeatClassService().findById(orderFlight.getSeatClassId());
            Price price = new PriceService().findByTwoId(plane, seatClass);
            OrderSeat orderSeat = new OrderSeatService().findByTwoId(flight, seatClass);
            if (plane == null || client == null || price == null || orderSeat == null) {
                throw new SQLException();
            }
            Integer numberFreeSeatsOnFlight = orderSeat.getNumberFreeSeats();
            if (numberFreeSeatsOnFlight > 0) {
                if (client.getMoney().compareTo(price.getPrice()) >= 0) {
                    client.setMoney(client.getMoney().divide(price.getPrice()));
                    orderSeat.setNumberFreeSeats(orderSeat.getNumberFreeSeats() - 1);
                    new ClientListService().update(client);
                    new OrderSeatService().update(orderSeat);
                    create(orderFlight);
                    return 1;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            connection.rollback();
        } finally {
            connection.setAutoCommit(true);
        }
        return 0;
    }
}
