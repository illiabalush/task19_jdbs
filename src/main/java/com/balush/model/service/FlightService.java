package com.balush.model.service;

import com.balush.model.DAO.implementation.FlightDAOImpl;
import com.balush.model.entity.Flight;
import com.balush.model.manager.ConnectionManager;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class FlightService {
    public List<Flight> findAll() throws SQLException {
        return new FlightDAOImpl().findAll();
    }

    public int deleteById(Integer deletedID) throws SQLException {
        return new FlightDAOImpl().deleteById(deletedID);
    }

    public Flight findById(Integer id) throws SQLException {
        return new FlightDAOImpl().findById(id);
    }

    public int update(Flight flight) throws SQLException {
        return new FlightDAOImpl().update(flight);
    }

    private int getNumberOfSeatsBySeatClassId(Flight flight, int id) throws SQLException {
        System.out.println(flight.getPlaneId());
        return new PlaneSeatService().findByTwoId(
                new PlaneService().findById(flight.getPlaneId()),
                new SeatClassService().findById(id)).getSeatNumber();
    }

    public void create(Flight flight) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try {
            connection.setAutoCommit(false);
            if (flight == null) {
                throw new SQLException();
            }
            new FlightDAOImpl().create(flight);
            flight = new FlightDAOImpl().getFlightByNumberFlight(flight.getFlightNumber());
        } catch (SQLException e) {
            e.printStackTrace();
            connection.rollback();
        } finally {
            connection.setAutoCommit(true);
        }
    }
}
