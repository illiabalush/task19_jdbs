package com.balush.model.service;

import com.balush.model.DAO.implementation.CityDAOImpl;
import com.balush.model.entity.City;

import java.sql.SQLException;
import java.util.List;

public class CityService {
    public List<City> findAll() throws SQLException {
        return new CityDAOImpl().findAll();
    }

    public int deleteById(Integer deletedID) throws SQLException {
        return new CityDAOImpl().deleteById(deletedID);
    }

    public int update(City city) throws SQLException {
        return new CityDAOImpl().update(city);
    }

    public int create(City city) throws SQLException {
        return new CityDAOImpl().create(city);
    }

    public City findById(Integer id) throws SQLException {
        return new CityDAOImpl().findById(id);
    }
}
