package com.balush.model.service;

import com.balush.model.DAO.implementation.PriceDAOImpl;
import com.balush.model.entity.Plane;
import com.balush.model.entity.Price;
import com.balush.model.entity.SeatClass;

import java.sql.SQLException;
import java.util.List;

public class PriceService {
    public List<Price> findAll() throws SQLException {
        return new PriceDAOImpl().findAll();
    }

    public Price findByTwoId(Plane plane, SeatClass seatClass) throws SQLException {
        return new PriceDAOImpl().findByTwoId(plane, seatClass);
    }

    public int deleteByTwoId(Plane plane, SeatClass seatClass) throws SQLException {
        return new PriceDAOImpl().deleteByTwoId(plane, seatClass);
    }

    public int create(Price entity) throws SQLException {
        return new PriceDAOImpl().create(entity);
    }

    public int update(Price entity) throws SQLException {
        return new PriceDAOImpl().update(entity);
    }
}
