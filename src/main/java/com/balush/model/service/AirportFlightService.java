package com.balush.model.service;

import com.balush.model.DAO.implementation.AirportFlightDAOImpl;
import com.balush.model.entity.AirportFlight;
import com.balush.model.entity.AirportList;
import com.balush.model.entity.Flight;

import java.sql.SQLException;
import java.util.List;

public class AirportFlightService {
    public List<AirportFlight> findAll() throws SQLException {
        return new AirportFlightDAOImpl().findAll();
    }

    public AirportFlight findByTwoId(AirportList airportList, Flight flight) throws SQLException {
        return new AirportFlightDAOImpl().findByTwoId(airportList, flight);
    }

    public int deleteByTwoId(AirportList airportList, Flight flight) throws SQLException {
        return new AirportFlightDAOImpl().deleteByTwoId(airportList, flight);
    }

    public int create(AirportFlight entity) throws SQLException {
        return new AirportFlightDAOImpl().create(entity);
    }
}
