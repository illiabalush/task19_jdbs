package com.balush.model.service;

import com.balush.model.DAO.implementation.PlaneDAOImpl;
import com.balush.model.entity.Plane;

import java.sql.SQLException;
import java.util.List;

public class PlaneService {
    public List<Plane> findAll() throws SQLException {
        return new PlaneDAOImpl().findAll();
    }

    public Plane findById(Integer id) throws SQLException {
        return new PlaneDAOImpl().findById(id);
    }

    public int deleteById(Integer deletedID) throws SQLException {
        return new PlaneDAOImpl().deleteById(deletedID);
    }

    public int update(Plane plane) throws SQLException {
        return new PlaneDAOImpl().update(plane);
    }

    public int create(Plane plane) throws SQLException {
        return new PlaneDAOImpl().update(plane);
    }
}
