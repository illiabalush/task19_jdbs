package com.balush.model.service;

import com.balush.model.DAO.implementation.SeatClassDAOImpl;
import com.balush.model.entity.SeatClass;

import java.sql.SQLException;
import java.util.List;

public class SeatClassService {
    public List<SeatClass> findAll() throws SQLException {
        return new SeatClassDAOImpl().findAll();
    }

    public SeatClass findById(Integer id) throws SQLException {
        return new SeatClassDAOImpl().findById(id);
    }

    public int deleteById(Integer deletedID) throws SQLException {
        return new SeatClassDAOImpl().deleteById(deletedID);
    }

    public int update(SeatClass seatClass) throws SQLException {
        return new SeatClassDAOImpl().update(seatClass);
    }

    public int create(SeatClass seatClass) throws SQLException {
        return new SeatClassDAOImpl().create(seatClass);
    }

}
