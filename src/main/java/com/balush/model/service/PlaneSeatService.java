package com.balush.model.service;

import com.balush.model.DAO.implementation.PlaneSeatDAOImpl;
import com.balush.model.entity.Plane;
import com.balush.model.entity.PlaneSeat;
import com.balush.model.entity.SeatClass;

import java.sql.SQLException;
import java.util.List;

public class PlaneSeatService {
    public List<PlaneSeat> findAll() throws SQLException {
        return new PlaneSeatDAOImpl().findAll();
    }

    public PlaneSeat findByTwoId(Plane plane, SeatClass seatClass) throws SQLException {
        return new PlaneSeatDAOImpl().findByTwoId(plane, seatClass);
    }

    public int deleteByTwoId(Plane plane, SeatClass seatClass) throws SQLException {
        return new PlaneSeatDAOImpl().deleteByTwoId(plane, seatClass);
    }

    public int create(PlaneSeat entity) throws SQLException {
        return new PlaneSeatDAOImpl().create(entity);
    }

    public int update(PlaneSeat entity) throws SQLException {
        return new PlaneSeatDAOImpl().update(entity);
    }
}
