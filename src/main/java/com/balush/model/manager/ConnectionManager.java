package com.balush.model.manager;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionManager {
    private static String url = null;
    private static String login = null;
    private static String password = null;

    private static Connection connection = null;

    private ConnectionManager() {
    }

    public static Connection getConnection() {
        if (connection == null) {
            synchronized (ConnectionManager.class) {
                if (connection == null) {
                    initializeConnection();
                }
            }
        }
        return connection;
    }

    private static void initializeConnection() {
        try (FileInputStream fileInputStream = new FileInputStream("src/main/resources/database.properties")) {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Properties properties = new Properties();
            properties.load(fileInputStream);
            url = properties.getProperty("database.host");
            login = properties.getProperty("database.login");
            password = properties.getProperty("database.password");
            connection = DriverManager.getConnection(url, login, password);
        } catch (IOException | SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
