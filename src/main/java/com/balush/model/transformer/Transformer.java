package com.balush.model.transformer;

import com.balush.model.entity.annotation.Column;
import com.balush.model.entity.annotation.ForeighKey;
import com.balush.model.entity.annotation.Table;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class Transformer<T> {
    private Class<T> clazz;

    public Transformer(Class<T> clazz) {
        this.clazz = clazz;
    }

    public Object convertResultSetToEntity(ResultSet resultSet) throws SQLException {
        Object entity = null;
        try {
            entity = clazz.getConstructor().newInstance();
            if (clazz.isAnnotationPresent(Table.class)) {
                Field[] fields = clazz.getDeclaredFields();
                for (Field field : fields) {
                    if (field.isAnnotationPresent(Column.class)) {
                        field.setAccessible(true);
                        setField(resultSet, field, entity);
                    } else if (field.isAnnotationPresent(ForeighKey.class)) {
                        field.setAccessible(true);
                        Class FKType = field.getType();
                        Object FK = FKType.getConstructor().newInstance();
                        field.set(entity, FK);
                        Field[] innerFKFields = FKType.getDeclaredFields();
                        for (Field innerFKField : innerFKFields) {
                            if (innerFKField.isAnnotationPresent(Column.class)) {
                                innerFKField.setAccessible(true);
                                setField(resultSet, innerFKField, FK);
                            }
                        }
                    }
                }
            }
        } catch (IllegalAccessException | InstantiationException
                | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return entity;
    }

    private void setField(ResultSet resultSet, Field field, Object entity)
            throws SQLException, IllegalAccessException {
        Column column = field.getAnnotation(Column.class);
        String columnName = column.name();
        Class fieldType = field.getType();
        if (resultSet.isBeforeFirst()) {
            resultSet.next();
        }
        if (fieldType == Integer.class) {
            field.set(entity, resultSet.getInt(columnName));
        } else if (fieldType == String.class) {
            field.set(entity, resultSet.getString(columnName));
        } else if (fieldType == Date.class) {
            field.set(entity, resultSet.getDate(columnName));
        } else if (fieldType == BigDecimal.class) {
            field.set(entity, resultSet.getBigDecimal(columnName));
        }
    }
}
