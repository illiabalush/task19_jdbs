package com.balush.model.entity;

import com.balush.model.entity.annotation.Column;
import com.balush.model.entity.annotation.ForeighKey;
import com.balush.model.entity.annotation.PrimaryKey;
import com.balush.model.entity.annotation.Table;

@Table(name = "PlaneSeat")
public class PlaneSeat {
    @PrimaryKey
    @ForeighKey
    @Column(name = "plane_id", length = 10)
    private Integer planeId;
    @PrimaryKey
    @ForeighKey
    @Column(name = "seat_class_id", length = 10)
    private Integer seatClassId;
    @Column(name = "seat_number", length = 10)
    private Integer seatNumber;

    public PlaneSeat() {
    }

    public PlaneSeat(Integer planeId, Integer seatClassId, Integer seatNumber) {
        this.planeId = planeId;
        this.seatClassId = seatClassId;
        this.seatNumber = seatNumber;
    }

    public Integer getPlaneId() {
        return planeId;
    }

    public void setPlaneId(Integer planeId) {
        this.planeId = planeId;
    }

    public Integer getSeatClassId() {
        return seatClassId;
    }

    public void setSeatClassId(Integer seatClassId) {
        this.seatClassId = seatClassId;
    }

    public Integer getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(Integer seatNumber) {
        this.seatNumber = seatNumber;
    }

    @Override
    public String toString() {
        return planeId + "\t" + seatClassId + "\t" + seatNumber;
    }
}
