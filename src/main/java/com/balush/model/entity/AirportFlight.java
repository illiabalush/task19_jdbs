package com.balush.model.entity;

import com.balush.model.entity.annotation.Column;
import com.balush.model.entity.annotation.ForeighKey;
import com.balush.model.entity.annotation.PrimaryKey;
import com.balush.model.entity.annotation.Table;

@Table(name = "AirportFlight")
public class AirportFlight {
    @PrimaryKey
    @ForeighKey
    @Column(name = "airport_id", length = 10)
    private Integer airportId;
    @PrimaryKey
    @ForeighKey
    @Column(name = "flight_id", length = 10)
    private Integer flightId;

    public AirportFlight() {
    }

    public AirportFlight(Integer airportId, Integer flightId) {
        this.airportId = airportId;
        this.flightId = flightId;
    }

    public Integer getAirportId() {
        return airportId;
    }

    public void setAirportId(Integer airportId) {
        this.airportId = airportId;
    }

    public Integer getFlightId() {
        return flightId;
    }

    public void setFlightId(Integer flightId) {
        this.flightId = flightId;
    }

    @Override
    public String toString() {
        return airportId + "\t" + flightId;
    }
}
