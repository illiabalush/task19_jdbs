package com.balush.model.entity;

import com.balush.model.entity.annotation.Column;
import com.balush.model.entity.annotation.PrimaryKey;
import com.balush.model.entity.annotation.Table;

@Table(name = "AirportListDAO")
public class AirportList {
    @PrimaryKey
    @Column(name = "id", length = 10)
    private Integer id;
    @Column(name = "airport_name")
    private String airportName;

    public AirportList() {
    }

    public AirportList(String airportName) {
        this.airportName = airportName;
    }

    public AirportList(Integer id, String airportName) {
        this.id = id;
        this.airportName = airportName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAirportName() {
        return airportName;
    }

    public void setAirportName(String airportName) {
        this.airportName = airportName;
    }

    @Override
    public String toString() {
        return id + "\t" + airportName;
    }
}
