package com.balush.model.entity;

import com.balush.model.entity.annotation.Column;
import com.balush.model.entity.annotation.ForeighKey;
import com.balush.model.entity.annotation.PrimaryKey;
import com.balush.model.entity.annotation.Table;

@Table(name = "OrderFlight")
public class OrderFlight {
    @PrimaryKey
    @Column(name = "id", length = 10)
    private Integer id;
    @ForeighKey
    @Column(name = "flight_id", length = 10)
    private Integer flightId;
    @PrimaryKey
    @Column(name = "seat_class_id", length = 10)
    private Integer seatClassId;
    @ForeighKey
    @Column(name = "client_id", length = 10)
    private Integer clientId;

    public OrderFlight() {
    }

    public OrderFlight(Integer flightId, Integer seatClassId, Integer clientId) {
        this.flightId = flightId;
        this.seatClassId = seatClassId;
        this.clientId = clientId;
    }

    public OrderFlight(Integer id, Integer flightId, Integer seatClassId, Integer clientId) {
        this.id = id;
        this.flightId = flightId;
        this.seatClassId = seatClassId;
        this.clientId = clientId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFlightId() {
        return flightId;
    }

    public void setFlightId(Integer flightId) {
        this.flightId = flightId;
    }

    public Integer getSeatClassId() {
        return seatClassId;
    }

    public void setSeatClassId(Integer seatClassId) {
        this.seatClassId = seatClassId;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    @Override
    public String toString() {
        return id + "\t" + flightId + "\t" + seatClassId + "\t" + clientId;
    }
}
