package com.balush.model.entity;

import com.balush.model.entity.annotation.Column;
import com.balush.model.entity.annotation.PrimaryKey;
import com.balush.model.entity.annotation.Table;

import java.math.BigDecimal;

@Table(name = "ClientList")
public class ClientList {
    @PrimaryKey
    @Column(name = "id", length = 10)
    private Integer id;
    @Column(name = "client_name")
    private String name;
    @Column(name = "client_surname")
    private String surname;
    @Column(name = "card_number", length = 50)
    private String cardNumber;
    @Column(name = "money", length = 50)
    private BigDecimal money;

    public ClientList() {
    }

    public ClientList(String name, String surname, String cardNumber, BigDecimal money) {
        this.name = name;
        this.surname = surname;
        this.cardNumber = cardNumber;
        this.money = money;
    }

    public ClientList(Integer id, String name, String surname, String cardNumber, BigDecimal money) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.cardNumber = cardNumber;
        this.money = money;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    @Override
    public String toString() {
        return id + "\t" + name  + "\t" + surname + "\t"
                + cardNumber + "\t\t" + money;
    }
}
