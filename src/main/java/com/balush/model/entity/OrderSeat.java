package com.balush.model.entity;

import com.balush.model.entity.annotation.Column;
import com.balush.model.entity.annotation.ForeighKey;
import com.balush.model.entity.annotation.PrimaryKey;
import com.balush.model.entity.annotation.Table;

@Table(name = "OrderSeatDAO")
public class OrderSeat {
    @PrimaryKey
    @ForeighKey
    @Column(name = "flight_id", length = 10)
    private Integer flightId;
    @PrimaryKey
    @ForeighKey
    @Column(name = "seat_class_id", length = 10)
    private Integer seatClassId;
    @Column(name = "number_free", length = 10)
    private Integer numberFreeSeats;

    public OrderSeat() {
    }

    public OrderSeat(Integer flightId, Integer seatClassId) {
        this.flightId = flightId;
        this.seatClassId = seatClassId;
    }

    public OrderSeat(Integer flightId, Integer seatClassId, Integer numberFreeSeats) {
        this.flightId = flightId;
        this.seatClassId = seatClassId;
        this.numberFreeSeats = numberFreeSeats;
    }

    public Integer getFlightId() {
        return flightId;
    }

    public void setFlightId(Integer flightId) {
        this.flightId = flightId;
    }

    public Integer getSeatClassId() {
        return seatClassId;
    }

    public void setSeatClassId(Integer seatClassId) {
        this.seatClassId = seatClassId;
    }

    public Integer getNumberFreeSeats() {
        return numberFreeSeats;
    }

    public void setNumberFreeSeats(Integer numberFreeSeats) {
        this.numberFreeSeats = numberFreeSeats;
    }

    @Override
    public String toString() {
        return flightId + "\t" + seatClassId + "\t" + numberFreeSeats;
    }
}
