package com.balush.model.entity;

import com.balush.model.entity.annotation.Column;
import com.balush.model.entity.annotation.ForeighKey;
import com.balush.model.entity.annotation.PrimaryKey;
import com.balush.model.entity.annotation.Table;

import java.text.SimpleDateFormat;
import java.util.Date;

@Table(name = "Flight")
public class Flight {
    @PrimaryKey
    @Column(name = "id", length = 10)
    private Integer id;
    @Column(name = "flight_number", length = 50)
    private String flightNumber;
    @ForeighKey
    @Column(name = "plane_id", length = 10)
    private Integer planeId;
    @ForeighKey
    @Column(name = "from_city_id", length = 10)
    private Integer cityFromId;
    @ForeighKey
    @Column(name = "to_city_id", length = 10)
    private Integer cityToId;
    @Column(name = "departure_date", length = 40)
    private Date departureDate;
    @Column(name = "arrival_date", length = 40)
    private Date arrivalDate;

    public Flight() {
    }

    public Flight(String flightNumber, Integer planeId, Integer cityFromId,
                  Integer cityToId, Date departureDate, Date arrivalDate) {
        this.flightNumber = flightNumber;
        this.planeId = planeId;
        this.cityFromId = cityFromId;
        this.cityToId = cityToId;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
    }

    public Flight(Integer id, String flightNumber, Integer planeId, Integer cityFromId,
                  Integer cityToId, Date departureDate, Date arrivalDate) {
        this.id = id;
        this.flightNumber = flightNumber;
        this.planeId = planeId;
        this.cityFromId = cityFromId;
        this.cityToId = cityToId;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public Integer getPlaneId() {
        return planeId;
    }

    public void setPlaneId(Integer planeId) {
        this.planeId = planeId;
    }

    public Integer getCityFromId() {
        return cityFromId;
    }

    public void setCityFromId(Integer cityFromId) {
        this.cityFromId = cityFromId;
    }

    public Integer getCityToId() {
        return cityToId;
    }

    public void setCityToId(Integer cityToId) {
        this.cityToId = cityToId;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    @Override
    public String toString() {
        return id + "\t" + flightNumber + "\t" + planeId + "\t" + cityFromId + "\t"
                + cityToId + "\t"
                + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(departureDate)
                + "\t"
                + new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(arrivalDate);
    }
}
