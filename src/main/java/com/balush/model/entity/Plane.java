package com.balush.model.entity;

import com.balush.model.entity.annotation.Column;
import com.balush.model.entity.annotation.PrimaryKey;
import com.balush.model.entity.annotation.Table;

@Table(name = "Plane")
public class Plane {
    @PrimaryKey
    @Column(name = "id", length = 10)
    private Integer id;
    @Column(name = "model", length = 50)
    private String model;

    public Plane() {
    }

    public Plane(Integer id, String model) {
        this.id = id;
        this.model = model;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public String toString() {
        return id + "\t" + model;
    }
}
