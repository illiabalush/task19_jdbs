package com.balush.model.entity;

import com.balush.model.entity.annotation.Column;
import com.balush.model.entity.annotation.PrimaryKey;
import com.balush.model.entity.annotation.Table;

@Table(name = "SeatClass")
public class SeatClass {
    @PrimaryKey
    @Column(name = "id", length = 10)
    private Integer id;
    @Column(name = "category", length = 100)
    private String category;

    public SeatClass() {
    }

    public SeatClass(Integer id, String category) {
        this.id = id;
        this.category = category;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return id + "\t" + category;
    }
}
