package com.balush.model.entity;

import com.balush.model.entity.annotation.Column;
import com.balush.model.entity.annotation.ForeighKey;
import com.balush.model.entity.annotation.PrimaryKey;
import com.balush.model.entity.annotation.Table;

import java.math.BigDecimal;

@Table(name = "Price")
public class Price {
    @PrimaryKey
    @ForeighKey
    @Column(name = "plane_id", length = 10)
    private Integer planeId;
    @PrimaryKey
    @ForeighKey
    @Column(name = "seat_class_id", length = 10)
    private Integer seatClassId;
    @Column(name = "price", length = 20)
    private BigDecimal price;

    public Price() {
    }

    public Price(Integer planeId, Integer seatClassId, BigDecimal price) {
        this.planeId = planeId;
        this.seatClassId = seatClassId;
        this.price = price;
    }

    public Integer getPlaneId() {
        return planeId;
    }

    public void setPlaneId(Integer planeId) {
        this.planeId = planeId;
    }

    public Integer getSeatClassId() {
        return seatClassId;
    }

    public void setSeatClassId(Integer seatClassId) {
        this.seatClassId = seatClassId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return planeId + "\t" + seatClassId + "\t" + price;
    }
}
